package com.task.app.controller;

import com.article.model.Article;
import com.task.app.service.SenderService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller
public class SenderController {
    @ModelAttribute("article")
    public Article getArticleObject() {
        return new Article();
    }


    @GetMapping("/")
    public String index() {
        return "index";
    }

    @GetMapping("new")
    public String add() {
        return "new";
    }

    @GetMapping("show")
    public String show() {
        SenderService.fetchArticles();
        return "show";
    }

    @PostMapping
    public String addArticle(@ModelAttribute("article") Article article) {
        SenderService.addArticle(article);
        return "index";
    }


}

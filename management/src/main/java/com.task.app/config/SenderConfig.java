package com.task.app.config;

import com.rabbit.configuration.RabbitConf;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SenderConfig {

    @Bean
    public Queue insertQueue() {
        return new Queue(RabbitConf.INSERT_QUEUE, false);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(RabbitConf.TOPIC_EXCHANGE_NAME);
    }

    @Bean
    Binding addBinding(TopicExchange exchange) {
        return BindingBuilder.bind(insertQueue()).to(exchange).with(RabbitConf.ROUTING_KEY_ADD);
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(jsonMessageConverter());
        return template;
    }

}

package com.task.app.service;

import com.article.model.Article;
import com.rabbit.configuration.RabbitConf;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

@Service
public class SenderService {

    private static RabbitTemplate rabbitTemplate;

    public SenderService(RabbitTemplate rbTemp) {
        rabbitTemplate = rbTemp;
    }


    public static void addArticle(Article article) {
        rabbitTemplate.convertAndSend(RabbitConf.TOPIC_EXCHANGE_NAME, "foo.bar.add", article);
    }

    public static void fetchArticles() {
//        rabbitTemplate.convertSendAndReceive(RabbitConf.TOPIC_EXCHANGE_NAME, "foo.bar.list", "");

    }
}

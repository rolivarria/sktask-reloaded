<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert Article</title>
</head>
<body>
	<h1>Insert Article</h1>
	<table>
		<tr>
			<td><a href="/" >Go to index</a> </td>
		</tr>
	</table>
	
	<form:form method="post" modelAttribute="article">
		<table>
			<tr>
				<td>Name:</td>
				<td><form:input path="name" type="text" name="name"/></td>
			</tr>
			<tr>
				<td>Valuable:</td>
				<td>
					<form:select path="valuable" name="valuable">
						<option value="true">Yes</option>
						<option value="false">No</option>
					</form:select>
				</td>
			</tr>
			<tr>
				<td>Date:</td>
				<td>
					<form:input path="date" type="date" name="date"/>
				</td>
			</tr>
			
			<tr>
				<td><input type="submit"/></td>
			</tr>
		</table>
	</form:form>
</body>
</html>
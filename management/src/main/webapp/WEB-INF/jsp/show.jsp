<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Show Articles</title>
</head>
<body>
	<h1>Article List</h1>
	<table>
		<tr>
			<td><a href="adminArticle?action=index" >Go to index</a> </td>
		</tr>
	</table>
	
	<table border="1">
		<tr>
		 <td> NAME</td>
		 <td> VALUABLE</td>
		 <td> DATE</td>
		</tr>
		<c:forEach var="article" items="${list}">
			<tr>
				<td><c:out value="${article.name}"/></td>
				<td><c:out value="${article.valuable}"/></td>
				<td><c:out value="${article.date}"/></td>
			</tr>
		</c:forEach>
	</table>
	
</body>
</html>
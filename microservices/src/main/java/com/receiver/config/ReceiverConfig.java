package com.receiver.config;

import com.article.model.Article;
import com.rabbit.configuration.RabbitConf;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.DefaultClassMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ReceiverConfig {

    @Bean
    public Queue listQueue() {
        return new Queue(RabbitConf.LIST_QUEUE, false);
    }

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(RabbitConf.TOPIC_EXCHANGE_NAME);
    }

    @Bean
    Binding addBinding(TopicExchange exchange) {
        return BindingBuilder.bind(listQueue()).to(exchange).with(RabbitConf.ROUTING_KEY_LIST);
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        Jackson2JsonMessageConverter jsonMessageConverter = new Jackson2JsonMessageConverter();
        jsonMessageConverter.setClassMapper(classMapper());
        return jsonMessageConverter;
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate template = new RabbitTemplate(connectionFactory);
        template.setMessageConverter(jsonMessageConverter());
        return template;
    }

    @Bean
    public DefaultClassMapper classMapper() {
        DefaultClassMapper classMapper = new DefaultClassMapper();
        Map<String, Class<?>> idClassMapping = new HashMap<>();
        idClassMapping.put("com.article.model.Article", Article.class);
        classMapper.setIdClassMapping(idClassMapping);
        return classMapper;
    }


}

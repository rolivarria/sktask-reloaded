package com.receiver.repository;

import com.article.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.LinkedList;


@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer> {

    @Query(value = "select * from addarticle(?1,?2,?3)", nativeQuery = true)
    Long addArticle(String name, boolean valuable, String date);

    @Query(value = "select * from listarticles()", nativeQuery = true)
    LinkedList<Article> listArticles();

}

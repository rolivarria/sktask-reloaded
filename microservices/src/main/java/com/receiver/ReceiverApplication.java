package com.receiver;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan(basePackages = "com.article.model")
public class ReceiverApplication {
    public static void main(String[] args) {

        SpringApplication.run(ReceiverApplication.class, args);
    }
}

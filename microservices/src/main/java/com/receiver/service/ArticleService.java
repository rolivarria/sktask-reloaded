package com.receiver.service;


import com.article.model.Article;
import com.receiver.repository.ArticleRepository;
import org.springframework.stereotype.Service;

import java.util.LinkedList;

@Service
public class ArticleService {

    private ArticleRepository repository;

    public ArticleService(ArticleRepository repository) {
        this.repository = repository;
    }

    public long addArticle(Article article) {
        return repository.addArticle(article.getName(), article.isValuable(), article.getDate());
    }

    public LinkedList<Article> listArticles() {
        return repository.listArticles();
    }


}

package com.receiver;

import com.article.model.Article;
import com.rabbit.configuration.RabbitConf;
import com.receiver.service.ArticleService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.LinkedList;


@Component
public class Receiver {
    org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Receiver.class);
    private ArticleService articleService;

    Receiver(ArticleService service) {
        this.articleService = service;
    }

    @RabbitListener(queues = {RabbitConf.INSERT_QUEUE})
    public void insertArticle(Article article) {
        logger.info(article.toString());
        long id = articleService.addArticle(article);
        logger.info(String.valueOf(id));
    }

//    @RabbitListener(queues = {RabbitConf.LIST_QUEUE})
//    public LinkedList listArticles() {
//        return articleService.listArticles();
//
//    }

}

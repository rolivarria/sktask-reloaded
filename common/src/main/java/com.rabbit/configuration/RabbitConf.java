package com.rabbit.configuration;


import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConf {

    public static final String TOPIC_EXCHANGE_NAME = "spring-boot-exchange";
    public static final String ROUTING_KEY_ADD = "foo.bar.add";
    public static final String ROUTING_KEY_LIST = "foo.bar.list";
    public static final String INSERT_QUEUE = "insert-queue";
    public static final String LIST_QUEUE = "list-queue";


    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(TOPIC_EXCHANGE_NAME);
    }


}

package com.article.model;

import javax.persistence.*;

@Entity
@Table(name="articles")
public class Article {
    @Id @GeneratedValue @Column
    private int id;
    @Column
    private String name;
    @Column
    private boolean valuable;
    @Column(name = "selected_date")
    private String date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isValuable() {
        return valuable;
    }

    public void setValuable(boolean valuable) {
        this.valuable = valuable;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString(){
        return "Name: " + this.getName() + " Valuable: " +this.isValuable()+ " Date: " +this.getDate();
    }


}

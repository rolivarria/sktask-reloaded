CREATE table articles(
  id SERIAL primary key,
  name VARCHAR(40),
  valuable BOOLEAN,
  selected_date DATE
);

CREATE or REPLACE FUNCTION addArticle(name character varying, valuable BOOLEAN, selected_date character varying, out id int) RETURNS integer as $id$
  BEGIN
    INSERT into articles(name, valuable, selected_date) values (name, valuable, to_date(selected_date,'YYYYMMDD'))
      RETURNING articles.id INTO id;
  END
$id$ LANGUAGE plpgsql;

CREATE FUNCTION listArticles() RETURNS setof articles as
$BODY$
BEGIN
  return query
    SELECT * from articles;
END;
$BODY$ LANGUAGE plpgsql;
